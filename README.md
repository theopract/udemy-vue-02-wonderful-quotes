
=======
# udemy-vue-02-wonderful-quotes

Second Course Project - Wonderful Quotes on udemy.com
Vue JS 2 - The Complete Guide (incl. Vue Router & Vuex) by Maximilian Schwarzmüller

## Build Setup

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build


